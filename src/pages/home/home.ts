import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	constructor(public navCtrl: NavController, public dataProvider: DataProvider) {

	}

	loadTheThingsSlow(){
	
		console.time("requests");

		this.dataProvider.getThingOne().then((res) => {

			let thingOne = res;
			console.log("thing one loaded");
	
			this.dataProvider.getThingTwo().then((res) => {

				let thingTwo = res;
				console.log("thing two loaded");

				this.dataProvider.getThingThree().then((res) => {

					let thingThree = res;
					console.log("thing three loaded");
					console.timeEnd("requests");

					console.log("All done!");
					console.log(thingOne);
					console.log(thingTwo);
					console.log(thingThree);

				});

			});

		});

	}

	loadTheThingsQuick(){

		console.time("requests");

		let promiseOne = this.dataProvider.getThingOne();
		let promiseTwo = this.dataProvider.getThingTwo();
		let promiseThree = this.dataProvider.getThingThree();

		Promise.all([promiseOne, promiseTwo, promiseThree]).then((values) => {
		
			console.timeEnd("requests");
			console.log("thingOne: ", values[0]);
			console.log("thingTwo: ", values[1]);
			console.log("thingThree: ", values[2]);

		});

	}

	loadObservables(){

		let observableOne = Observable.fromPromise(this.dataProvider.getThingOne());
		let observableTwo = Observable.fromPromise(this.dataProvider.getThingTwo());
		let observableThree = Observable.fromPromise(this.dataProvider.getThingThree());

		Observable.forkJoin([observableOne, observableTwo, observableThree]).subscribe(res => {
			console.log(res);
		});

	}

}
